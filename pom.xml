<project xmlns="http://maven.apache.org/POM/4.0.0"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>

	<groupId>org.prelle</groupId>
	<artifactId>gdrivefs</artifactId>
	<version>0.3-SNAPSHOT</version>
	<name>Google Drive Filesystem</name>
	<description>Java NIO implementation of Google Drive </description>

	<properties>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
		<log4j.version>2.10.0</log4j.version>
		<google.version>1.23.0</google.version>
	</properties>

	<licenses>
		<license>
			<name>GNU lesser general public license 2.1</name>
			<url>https://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt</url>
			<distribution>repo</distribution>
		</license>
	</licenses>

	<organization>
		<name>Stefan Prelle</name>
		<url>http://www.prelle.org/</url>
	</organization>

	<dependencies>
		<dependency>
			<groupId>org.apache.logging.log4j</groupId>
			<artifactId>log4j-api</artifactId>
			<version>${log4j.version}</version>
		</dependency>
		<dependency>
			<groupId>junit</groupId>
			<artifactId>junit</artifactId>
			<version>[4.12,)</version>
		</dependency>

		<!-- Google Drive -->
		<dependency>
			<groupId>com.google.apis</groupId>
			<artifactId>google-api-services-drive</artifactId>
			<version>v3-rev101-${google.version}</version>
		</dependency>
		<dependency>
			<groupId>com.google.api-client</groupId>
			<artifactId>google-api-client</artifactId>
			<version>${google.version}</version>
		</dependency>
		<dependency>
			<groupId>com.google.http-client</groupId>
			<artifactId>google-http-client</artifactId>
			<version>${google.version}</version>
		</dependency>
		<dependency>
			<groupId>com.google.http-client</groupId>
			<artifactId>google-http-client-jackson2</artifactId>
			<version>${google.version}</version>
		</dependency>
		<dependency>
			<groupId>com.google.oauth-client</groupId>
			<artifactId>google-oauth-client-jetty</artifactId>
			<version>${google.version}</version>
		</dependency>
	</dependencies>
	
	
	<build>
		<plugins>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-compiler-plugin</artifactId>
				<version>3.8.0</version>
				<configuration>
					<release>11</release>
				</configuration>
			</plugin>

			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-jar-plugin</artifactId>
				<version>3.0.2</version>
				<configuration>
					<archive>
						<manifest>
							<addDefaultImplementationEntries>false</addDefaultImplementationEntries>
							<addDefaultSpecificationEntries>false</addDefaultSpecificationEntries>
						</manifest>
						<manifestEntries>
							<Implementation-Title>${project.name}</Implementation-Title>
							<Implementation-Vendor>${project.organization.name}</Implementation-Vendor>
							<Build-Info>Java ${java.version} on ${os.name}</Build-Info>
						</manifestEntries>
					</archive>
				</configuration>
			</plugin>

			<plugin>
				<artifactId>maven-deploy-plugin</artifactId>
				<version>2.8.2</version>
				<executions>
					<execution>
						<id>default-deploy</id>
						<phase>deploy</phase>
						<goals>
							<goal>deploy</goal>
						</goals>
					</execution>
				</executions>
			</plugin>

			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-release-plugin</artifactId>
				<version>2.5.3</version>
				<configuration>
					<localCheckout>true</localCheckout>
					<pushChanges>false</pushChanges>
					<mavenExecutorId>forked-path</mavenExecutorId>
					<arguments>-Dgpg.passphrase=${gpg.passphrase}</arguments>
				</configuration>
				<dependencies>
					<dependency>
						<groupId>org.apache.maven.scm</groupId>
						<artifactId>maven-scm-provider-gitexe</artifactId>
						<version>1.9.5</version>
					</dependency>
				</dependencies>
			</plugin>

			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-source-plugin</artifactId>
				<version>2.2.1</version>
				<executions>
					<execution>
						<id>attach-sources</id>
						<goals>
							<goal>jar-no-fork</goal>
						</goals>
					</execution>
				</executions>
			</plugin>

			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-javadoc-plugin</artifactId>
				<version>3.0.1</version>
				<executions>
					<execution>
						<id>attach-javadocs</id>
						<goals>
							<goal>jar</goal>
						</goals>
					</execution>
				</executions>
			</plugin>

			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-gpg-plugin</artifactId>
				<version>1.6</version>
				<executions>
					<execution>
						<id>sign-artifacts</id>
						<phase>verify</phase>
						<configuration>
							<gpgArguments>
								<arg>--no-tty</arg>
								<arg>--batch</arg>
								<arg>--yes</arg>
							</gpgArguments>
						</configuration>
						<goals>
							<goal>sign</goal>
						</goals>
					</execution>
				</executions>
			</plugin>
			<plugin>
				<groupId>org.sonatype.plugins</groupId>
				<artifactId>nexus-staging-maven-plugin</artifactId>
				<version>1.6.7</version>
				<extensions>true</extensions>
				<configuration>
					<serverId>ossrh</serverId>
					<nexusUrl>https://oss.sonatype.org/</nexusUrl>
					<autoReleaseAfterClose>true</autoReleaseAfterClose>
				</configuration>
			</plugin>
		</plugins>
	</build>

	<ciManagement>
		<system>Bamboo</system>
		<url>http://build.rgpframework.de/</url>
	</ciManagement>
	<issueManagement>
		<system>JIRA</system>
		<url>http://jira.rpgframework.de:18080/</url>
	</issueManagement>
	<scm>
		<connection>scm:git:git://github.com/dexecutor/dependent-tasks-executor.git</connection>
		<developerConnection>scm:git:git@github.com:dexecutor/dexecutor.git</developerConnection>
		<url>https://github.com/dexecutor/dependent-tasks-executor</url>
		<tag>HEAD</tag>
	</scm>

	<distributionManagement>
		<snapshotRepository>
			<id>ossrh</id>
			<url>https://oss.sonatype.org/content/repositories/snapshots</url>
		</snapshotRepository>
		<repository>
			<id>ossrh</id>
			<url>https://oss.sonatype.org/service/local/staging/deploy/maven2</url>
		</repository>
	</distributionManagement>
	
</project>