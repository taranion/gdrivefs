package foo;

import java.net.URI;
import java.nio.file.DirectoryNotEmptyException;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Example3 {
	
	private final static Logger logger = LogManager.getLogger("app");

	//-----------------------------------------------------------------
	public static void main(String[] args) throws Exception {
		String uri = String.format("google://default");
		logger.info("Connect "+uri);
		Map<String, String> env = new HashMap<>();
		FileSystem fs = FileSystems.newFileSystem(URI.create(uri), env);
//		FileSystem fs = FileSystems.getDefault();
		logger.debug("fs = "+fs);
		
		Path remoteRoot = fs.getRootDirectories().iterator().next();
//		remoteRoot = remoteRoot.resolve("tmp");
		logger.debug("\nremote0 = "+remoteRoot);
		Path remote1 = remoteRoot.resolve("RPGFramework");
		logger.debug("\nremote1 = "+remote1);
		Path remote2 = remoteRoot.resolve("RPGFramework/foo");
		logger.debug("\nremote2 = "+remote2);
		Path remote3 = remoteRoot.resolve("RPGFramework/foo/bar");
		logger.debug("\nremote3 = "+remote3);
		logger.debug("\nremote3 = "+remote3.getParent().getFileName());
		Iterator<Path> it = remote3.iterator();
		while (it.hasNext()) {
			logger.debug("- "+it.next());
		}
		for (int i=0; i<remote3.getNameCount(); i++)
			logger.debug("* "+remote3.getName(i));
		try {
			Files.createDirectory(remote3);  // NoSuchFile
			logger.error("Missing NoSuchFileException");
			System.exit(0);
		} catch (NoSuchFileException e) {
			logger.warn(e.toString());
		}
		Files.createDirectories(remote3); 
		try {
			Files.delete(remote1); // DirectoryNotEmpty
			logger.error("Missing DirectoryNotEmptyException");
			System.exit(0);
		} catch (DirectoryNotEmptyException e) {
			logger.warn(e.toString());
		}
		Files.delete(remote3); 
		Files.delete(remote2); 
		Files.delete(remote1); 
		
		System.exit(0);
	}

}
