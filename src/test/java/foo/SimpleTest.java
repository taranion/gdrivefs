/**
 * 
 */
package foo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.BufferedWriter;
import java.io.IOException;
import java.net.URI;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.FileStore;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.nio.file.attribute.FileTime;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.prelle.gdrivefs.GoogleDriveFSConstants;

/**
 * @author spr
 *
 */
public class SimpleTest {

	private final static boolean USE_LOCALFS_INSTEAD = false;
	
	private final static Logger logger = LogManager.getLogger("test");

	private FileSystem fs;
	private Path basePath;
	private List<Path> toDeleteOnExit;
	
	//-------------------------------------------------------------------
	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	//-------------------------------------------------------------------
	@Before
	public void setUp() throws Exception {
		toDeleteOnExit = new ArrayList<>();
		if (USE_LOCALFS_INSTEAD) {
			fs = FileSystems.getFileSystem(new URI("file:///"));			
		} else {
			String uri = String.format("google:/");
			Map<String, Object> env = new HashMap<>();
			env.put(GoogleDriveFSConstants.SCOPE, GoogleDriveFSConstants.SCOPE_FILE);
			env.put(GoogleDriveFSConstants.APPLICATION_NAME, "GDriveFSUnitTest");
			env.put(GoogleDriveFSConstants.CLIENT_SECRETS, ClassLoader.getSystemResourceAsStream("client_secrets.json"));
			fs = FileSystems.newFileSystem(URI.create(uri), env);
		}
		
		if (fs==FileSystems.getDefault()) {
			basePath = Files.createTempDirectory("gdrivefs-unittest");
			basePath.toFile().deleteOnExit();
		} else {
//			basePath = fs.getPath("/");
			basePath = Paths.get("/");
		}
	}

	//-------------------------------------------------------------------
	@After
	public void tearDown() throws Exception {
		for (Path deleteMe : toDeleteOnExit) {
			try {
				logger.debug("delete "+deleteMe);
				Files.delete(deleteMe);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		if (fs!=FileSystems.getDefault()) {
			logger.warn("Calling close");
			fs.close();
		}
	}

	//-------------------------------------------------------------------
	@Test
	public void listFileStores() {
		logger.debug("START: listFileStores---------------------------------------------");
		int count=0;
		for (FileStore store : fs.getFileStores()) {
			try {
				logger.debug ("  found FileStore "+store+" with "+store.getUnallocatedSpace()+" of "+store.getTotalSpace());
			} catch (IOException e) {
				e.printStackTrace();
			}
			count++;
		}
		assertTrue("Should have found at least one filestore", count>0);
		logger.debug("STOP : listFileStores---------------------------------------------");
	}

	//-------------------------------------------------------------------
	@Test
	public void listRootDirectories() {
		logger.debug("START: listRootDirectories---------------------------------------------");
		int count=0;
		for (Path root : fs.getRootDirectories()) {
			logger.trace ("  found root directory "+root);
			count++;
		}
		assertTrue("Should have found at least one root directory", count>0);
		logger.debug("STOP : listRootDirectories---------------------------------------------");
	}

	//-------------------------------------------------------------------
	@Test
	public void listRootDirectoryContents() throws IOException {
		logger.debug("START: listRootDirectoryContents---------------------------------------------");
		Path firstRoot = fs.getRootDirectories().iterator().next();
		logger.debug("listing content of "+firstRoot);
		for (Path tmp : Files.newDirectoryStream(firstRoot)) {
			logger.trace ("  * "+tmp);
		}
		logger.debug("STOP : listRootDirectoryContents---------------------------------------------");
	}

	//-------------------------------------------------------------------
	@Test
	public void checkRootPath() throws IOException {
		logger.debug("START: checkRootPath---------------------------------------------");
		try {
			Path firstRoot = fs.getRootDirectories().iterator().next();
			assertEquals(firstRoot, firstRoot.getRoot());
			assertSame(firstRoot, firstRoot.getRoot());
			
			// Now check for subdirectory
			Path subDir = firstRoot.resolve("bla");
			assertNotNull(subDir);
			
			assertEquals(firstRoot, subDir.getRoot());
//			assertSame(firstRoot, subDir.getRoot());
			
		} finally {
			logger.debug("STOP : checkRootPath---------------------------------------------");
		}
	}

	//-------------------------------------------------------------------
	@Test
	public void createFile() throws IOException {
		logger.debug("START: createFile---------------------------------------------");
		try {
		assertNotNull(basePath);
		Path firstRoot = fs.getRootDirectories().iterator().next();
		Path folder = firstRoot.resolve(basePath);
		
		UUID generated = UUID.randomUUID();
		Path file = folder.resolve(generated.toString());
		logger.debug("  create "+file);
		
		Path created = Files.createFile(file);
		assertEquals("Path result of creation should be identical to requested path", created, file);

		// Check file exists
		boolean found = false;
		for (Path tmp : Files.newDirectoryStream(folder)) {
			if (tmp.getFileName().toString().equals(generated.toString()))
				found = true;
		}
		assertTrue("Created file does not appear in directory", found);

		toDeleteOnExit.add(created);
		} finally {
			logger.debug("STOP : createFile---------------------------------------------");
		}
	}

	//-------------------------------------------------------------------
	@Test
	public void deleteFile() throws IOException {
		logger.debug("START: deleteFile---------------------------------------------");
		Path firstRoot = fs.getRootDirectories().iterator().next();
		UUID generated = UUID.randomUUID();
		Path file = firstRoot.resolve(basePath.resolve(generated.toString()));
		
		Path created = Files.createFile(file);
		assertEquals("Path result of creation should be identical to requested path", created, file);
		Files.delete(created);

		// Check file exists
		boolean found = false;
		for (Path tmp : Files.newDirectoryStream(basePath)) {
			if (tmp.getFileName().toString().equals(generated.toString()))
				found = true;
		}
		assertFalse("Created and afterwards deleted file does still appear in directory", found);
		
		logger.debug("STOP : deleteFile---------------------------------------------");
	}

	//-------------------------------------------------------------------
	@Test
	public void createAndFillFile() throws IOException {
		logger.debug("START: createAndFillFile---------------------------------------------");
		assertNotNull(basePath);
		Path firstRoot = fs.getRootDirectories().iterator().next();
		Path folder = firstRoot.resolve(basePath);
		
		UUID generated = UUID.randomUUID();
		Path file = folder.resolve(generated.toString());
		logger.debug("  create "+file);
		
		BufferedWriter writer = Files.newBufferedWriter(file, StandardOpenOption.WRITE, StandardOpenOption.CREATE_NEW);
		writer.append("Dies sind Testdaten mit Umlauten (äöü ÄÖÜ ß)");
		writer.flush();

		// Check file size
		System.err.println("Filesize = "+ Files.size(file));
		assertTrue("Filesize does not match", Files.size(file)>50);

		toDeleteOnExit.add(file);
		logger.debug("STOP : createAndFillFile---------------------------------------------");
	}

	//-------------------------------------------------------------------
	@Test
	public void createFileAgain() throws IOException {
		logger.debug("START: createFileAgain---------------------------------------------");
		try {
			assertNotNull(basePath);
			Path firstRoot = fs.getRootDirectories().iterator().next();
			Path folder = firstRoot.resolve(basePath);

			UUID generated = UUID.randomUUID();
			Path file1 = folder.resolve(generated.toString());
			Path file2 = folder.resolve(generated.toString());

			Path created = Files.createFile(file1);
			toDeleteOnExit.add(created);
			assertEquals("Path result of creation should be identical to requested path", created, file1);
			try {
				Files.createFile(file2);
				fail("FailAlreadyExistsException expected");
			} catch (FileAlreadyExistsException e) {
			}

		} finally {
			logger.debug("STOP : createFileAgain---------------------------------------------");
		}

	}

	//-------------------------------------------------------------------
	@Test
	public void getModificationTime() throws IOException {
		logger.debug("START: getModificationTime---------------------------------------------");
		try {
			assertNotNull(basePath);
			Path firstRoot = fs.getRootDirectories().iterator().next();
			Path folder = firstRoot.resolve(basePath);

			UUID generated = UUID.randomUUID();
			Path file = folder.resolve(generated.toString());
			Path created = Files.createFile(file);
			toDeleteOnExit.add(created);

			FileTime creationTime = Files.getLastModifiedTime(created);
			assertNotNull(creationTime);
		} finally {
			logger.debug("STOP : getModificationTime---------------------------------------------");
		}
	}

	//-------------------------------------------------------------------
	@Test
	public void setModificationTime() throws IOException {
		logger.debug("START: setModificationTime---------------------------------------------");
		try {
			assertNotNull(basePath);
			Path firstRoot = fs.getRootDirectories().iterator().next();
			Path folder = firstRoot.resolve(basePath);
			Instant timeToSet = Instant.parse("2007-12-03T10:15:30.00Z");

			UUID generated = UUID.randomUUID();
			Path file = folder.resolve(generated.toString());
			Path created = Files.createFile(file);
			toDeleteOnExit.add(created);

			// Get old time
			FileTime creationTime = Files.getLastModifiedTime(created);
			assertNotNull(creationTime);
			logger.debug("Creation time: "+creationTime);

			// Set new time
			Files.setLastModifiedTime(created, FileTime.from(timeToSet));
			
			// Check time is set
			FileTime modifiedTime = Files.getLastModifiedTime(created);
			assertNotNull(modifiedTime);
			logger.debug("Modified time: "+modifiedTime);
			assertEquals(modifiedTime.toInstant(), timeToSet);

		} finally {
			logger.debug("STOP : setModificationTime---------------------------------------------");
		}
	}

}
