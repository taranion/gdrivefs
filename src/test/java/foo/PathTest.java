package foo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.prelle.gdrivefs.GoogleDrivePath;
import org.prelle.gdrivefs.PathElement;
import org.prelle.gdrivefs.Type;

public class PathTest {

	private final static boolean USE_LOCALFS_INSTEAD = false;

	private Path nonAbsolute; 
	private Path absolute; 
	private Path absolute2; 
	
	//-------------------------------------------------------------------
	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	//-------------------------------------------------------------------
	@Before
	public void setUp() throws URISyntaxException {
		if (USE_LOCALFS_INSTEAD) {
			nonAbsolute = Paths.get("hallo", "welt");
			absolute    = Paths.get(new URI("file:///hello/world"));			
			absolute2   = Paths.get(new URI("file:///hello/yourself"));			
		} else {
			nonAbsolute = new GoogleDrivePath(null, Arrays.asList(
					new PathElement("hallo"),
					new PathElement("welt")));
			absolute = new GoogleDrivePath(null, Arrays.asList(
					new PathElement(Type.ROOT),
					new PathElement("hello"),
					new PathElement("world")));
			absolute2 = new GoogleDrivePath(null, Arrays.asList(
					new PathElement(Type.ROOT),
					new PathElement("hello"),
					new PathElement("yourself")));
		}
	}

	//-------------------------------------------------------------------
	@Test
	public void testIsAbsolute() {
		assertTrue(absolute.isAbsolute());
		assertFalse(nonAbsolute.isAbsolute());
	}

	//-------------------------------------------------------------------
	@Test
	public void testGetRoot() {
		assertNotNull(absolute.getRoot());
		assertNull(nonAbsolute.getRoot());
	}

	//-------------------------------------------------------------------
	@Test
	public void testGetFileName() {
		assertNotNull(nonAbsolute.getFileName());
		assertNotNull(absolute.getFileName());
		assertEquals("welt", nonAbsolute.getFileName().toString());
		assertEquals("world", absolute.getFileName().toString());
	}

	//-------------------------------------------------------------------
	@Test
	public void testGetParent() {
		assertNotNull(nonAbsolute.getParent());
		assertNotNull(absolute.getParent());
		assertEquals("hallo", nonAbsolute.getParent().getFileName().toString());
		assertEquals("hello", absolute.getParent().getFileName().toString());
		
		assertEquals(absolute.getParent(), absolute2.getParent());
	}

	//-------------------------------------------------------------------
	@Test
	public void testGetNameCount() {
		assertEquals(2, nonAbsolute.getNameCount());
		assertEquals(2, absolute.getNameCount());
	}

	//-------------------------------------------------------------------
	@Test
	public void testGetName() {
		assertEquals("hallo", nonAbsolute.getName(0).toString());
		assertEquals("welt", nonAbsolute.getName(1).toString());
		assertEquals("hello", absolute.getName(0).toString());
		assertEquals("world", absolute.getName(1).toString());
	}

//	//-------------------------------------------------------------------
//	@Test
//	public void testSubpath() {
//		assertNotNull(absolute.subpath(1, 2));
//		assertNotNull(nonAbsolute.subpath(1, 2));
//		assertEquals("world", absolute.subpath(1, 2).toString());
//	}

//	@Test
//	public void testStartsWithPath() {
//		fail("Not yet implemented");
//	}
//
//	@Test
//	public void testStartsWithString() {
//		fail("Not yet implemented");
//	}
//
//	@Test
//	public void testEndsWithPath() {
//		fail("Not yet implemented");
//	}
//
//	@Test
//	public void testEndsWithString() {
//		fail("Not yet implemented");
//	}
//
//	@Test
//	public void testNormalize() {
//		fail("Not yet implemented");
//	}

	//-------------------------------------------------------------------
	@Test
	public void testResolvePath() {
		assertEquals(nonAbsolute, nonAbsolute.getParent().resolve(Paths.get("welt")));
		assertEquals(absolute, absolute2.getParent().resolve(Paths.get("world")));
	}

//	//-------------------------------------------------------------------
//	@Test
//	public void testResolveString() {
//		assertEquals(nonAbsolute, nonAbsolute.getParent().resolve("welt"));
//		assertEquals(absolute, absolute2.getParent().resolve("world"));
//	}

//	@Test
//	public void testResolveSiblingPath() {
//		fail("Not yet implemented");
//	}
//
//	@Test
//	public void testResolveSiblingString() {
//		fail("Not yet implemented");
//	}
//
//	@Test
//	public void testToUri() {
//		fail("Not yet implemented");
//	}
//
//	@Test
//	public void testToAbsolutePath() {
//		fail("Not yet implemented");
//	}
//
//	@Test
//	public void testToRealPath() {
//		fail("Not yet implemented");
//	}
//
//	@Test
//	public void testToFile() {
//		fail("Not yet implemented");
//	}

}
