package foo;

import java.net.URI;
import java.nio.file.DirectoryStream;
import java.nio.file.FileStore;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.gdrivefs.GoogleDriveFSConstants;

public class Example {
	
	private final static Logger logger = LogManager.getLogger("app");

	//-----------------------------------------------------------------
	public static void main(String[] args) throws Exception {
		String uri = String.format("google:/RPGFramework");
		logger.info("Connect "+uri);
		Map<String, Object> env = new HashMap<>();
		env.put(GoogleDriveFSConstants.SCOPE, GoogleDriveFSConstants.SCOPE_FILE);
		env.put(GoogleDriveFSConstants.APPLICATION_NAME, "GDriveFSUnitTest");
		env.put(GoogleDriveFSConstants.CLIENT_SECRETS, ClassLoader.getSystemResourceAsStream("client_secrets.json"));
		FileSystem fs = FileSystems.newFileSystem(URI.create(uri), env);
		logger.debug("fs = "+fs);
		
		if (logger.isInfoEnabled()) {
			logger.info("Found the following file stores:");
			logger.info("================================");
			for (FileStore store : fs.getFileStores()) {
				logger.info ("* "+store);
				logger.debug("  "+store.getTotalSpace()+" total,  "+store.getUnallocatedSpace()+" free");
			}

			logger.info("Found the following root directories:");
			logger.info("=====================================");
			for (Path dir : fs.getRootDirectories()) {
				logger.info("* "+dir);
				DirectoryStream<Path> stream = Files.newDirectoryStream(dir);
				for (Path child : stream) {
					logger.info("..* "+child);
					if (Files.isDirectory(child)) {
						DirectoryStream<Path> stream2 = Files.newDirectoryStream(child);
						for (Path child2 : stream2) {
							logger.info("....* "+child2);
						}
						
					}
				}
			}
		}
		
//		// Create a new file
//		Path path = fs.getPath("QSCom:/Test1/Test2/test.txt");
//		logger.debug("Path = "+path);
//		
//		Files.createDirectories(path.getParent());
//		
//		// Check if it exists
//		boolean exists = Files.exists(path);
//		logger.debug(path+" exists = "+exists);
//		// Create it
//		if (!exists)
//			path = Files.createFile(path);
//		
//		Files.write(path, ("Hello world").getBytes());
//		
//		fs.close();
		
		System.exit(0);
	}

}
