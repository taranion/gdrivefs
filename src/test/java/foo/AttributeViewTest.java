/**
 * 
 */
package foo;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.UserDefinedFileAttributeView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.prelle.gdrivefs.GoogleDriveFSConstants;

/**
 * @author spr
 *
 */
public class AttributeViewTest {

	private final static boolean USE_LOCALFS_INSTEAD = true;
	
	private final static Logger logger = LogManager.getLogger("google.drive.prov");

	private FileSystem fs;
	private Path basePath;
	private List<Path> toDeleteOnExit;
	
	//-------------------------------------------------------------------
	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	//-------------------------------------------------------------------
	@Before
	public void setUp() throws Exception {
		toDeleteOnExit = new ArrayList<>();
		if (USE_LOCALFS_INSTEAD) {
			fs = FileSystems.getFileSystem(new URI("file:///"));			
		} else {
			String uri = String.format("google:/");
			Map<String, Object> env = new HashMap<>();
			env.put(GoogleDriveFSConstants.SCOPE, GoogleDriveFSConstants.SCOPE_FILE);
			env.put(GoogleDriveFSConstants.APPLICATION_NAME, "GDriveFSUnitTest");
			env.put(GoogleDriveFSConstants.CLIENT_SECRETS, ClassLoader.getSystemResourceAsStream("client_secrets.json"));
			fs = FileSystems.newFileSystem(URI.create(uri), env);
		}
		
		if (fs==FileSystems.getDefault()) {
			basePath = Files.createTempDirectory("gdrivefs-unittest");
			basePath.toFile().deleteOnExit();
		} else {
//			basePath = fs.getPath("/");
			basePath = Paths.get("/");
		}
	}

	//-------------------------------------------------------------------
	@After
	public void tearDown() throws Exception {
		for (Path deleteMe : toDeleteOnExit) {
			try {
				logger.debug("delete "+deleteMe);
				Files.delete(deleteMe);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		if (fs!=FileSystems.getDefault()) {
			logger.warn("Calling close");
			fs.close();
		}
	}

	//-------------------------------------------------------------------
	@Test
	public void availableViews() {
		for (String fileAttributeView : fs.supportedFileAttributeViews()) {
		    System.out.println("file system supports: " + fileAttributeView);
		}
	}
	
	//-------------------------------------------------------------------
	@Test
	public void setCustomAttribute() throws IOException {
		logger.debug("START: setCustomAttribute---------------------------------------------");
		try {
			assertNotNull(basePath);
			Path firstRoot = fs.getRootDirectories().iterator().next();
			Path folder = firstRoot.resolve(basePath);

			UUID generated = UUID.randomUUID();
			Path file = folder.resolve(generated.toString());
			file = Paths.get("/home/prelle/foobar");
			Path created = Files.createFile(file);
			toDeleteOnExit.add(created);

			// User Attribute View
			UserDefinedFileAttributeView view = Files.getFileAttributeView(created, UserDefinedFileAttributeView.class);
			assertNotNull(view);		
			System.out.println("Before = "+view.list());
			assertTrue(view.list().isEmpty());
			
			// Add user defined value
			System.out.println("Bytes written = "+view.write("xattr-foo", StandardCharsets.UTF_8.encode("Hallo Welt")));
			System.out.println("After  = "+view.list());
			System.out.println("bla  = "+new String((byte[])Files.getAttribute(file, "user:xattr-foo")));

		} finally {
			logger.debug("STOP : setCustomAttribute---------------------------------------------");
		}
	}

}
