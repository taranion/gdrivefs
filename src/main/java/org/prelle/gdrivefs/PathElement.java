package org.prelle.gdrivefs;

import com.google.api.services.drive.model.File;

public class PathElement {
	
	Type type;
	String name;
	/** Optional. Only present if resolved */
	String fileId;
	File internalFile;
	
	public PathElement(Type type) {
		this.type = type;
	}
	
	public PathElement(String name) {
		this.type = Type.FILE_OR_FOLDER;
		this.name = name;
	}
	public PathElement(PathElement toClone) {
		this.type = toClone.type;
		this.name = toClone.name;
		this.fileId = toClone.getFileId();
	}
	public String toString() {
		String tmp = (fileId!=null)?"(B)":"(U)";
		switch (type) {
		case ROOT: return tmp+"ROOT";
		case SELF_REFERENCE: return tmp+".";
		case PARENT_REFERENCE: return tmp+"..";
		default: return tmp+name;
		}
	}
	@Override
	public int hashCode() {
		return (type+"-"+name).hashCode();
	}
	@Override
	public boolean equals(Object o) {
		if (o instanceof PathElement) {
			PathElement other = (PathElement)o;
			if (type!=other.type) return false;
			return String.valueOf(name).equals(String.valueOf(other.name));
		}
		return false;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the fileId
	 */
	public String getFileId() {
		return fileId;
	}

	//-------------------------------------------------------------------
	/**
	 * @param fileId the fileId to set
	 */
	public void setFileId(String fileId) {
		this.fileId = fileId;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the internalFile
	 */
	public File getInternalFile() {
		return internalFile;
	}

	//-------------------------------------------------------------------
	/**
	 * @param internalFile the internalFile to set
	 */
	public void setInternalFile(File internalFile) {
		this.internalFile = internalFile;
	}
}