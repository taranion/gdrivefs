/**
 * 
 */
package org.prelle.gdrivefs;

import java.io.IOException;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author prelle
 *
 */
public class GoogleDriveWatchService implements WatchService {
	
	private final static Logger logger = LogManager.getLogger("google.drive");

	//-------------------------------------------------------------------
	public GoogleDriveWatchService() {
		// TODO Auto-generated constructor stub
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.nio.file.WatchService#close()
	 */
	@Override
	public void close() throws IOException {
		// TODO Auto-generated method stub
		logger.warn("TODO: close()");
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.nio.file.WatchService#poll()
	 */
	@Override
	public WatchKey poll() {
		// TODO Auto-generated method stub
		logger.warn("TODO: poll()");
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.nio.file.WatchService#poll(long, java.util.concurrent.TimeUnit)
	 */
	@Override
	public WatchKey poll(long timeout, TimeUnit unit) throws InterruptedException {
		// TODO Auto-generated method stub
		logger.warn("TODO: poll(..)");
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.nio.file.WatchService#take()
	 */
	@Override
	public WatchKey take() throws InterruptedException {
		// TODO Auto-generated method stub
		logger.warn("TODO: take(..)");
		synchronized (this) {
			logger.warn("Wait indefinetly");
			this.wait();
		}
		return null;
	}

}
