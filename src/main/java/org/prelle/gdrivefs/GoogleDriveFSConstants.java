/**
 * 
 */
package org.prelle.gdrivefs;

import com.google.api.services.drive.DriveScopes;

/**
 * @author spr
 *
 */
public interface GoogleDriveFSConstants {

	public final static String APPLICATION_NAME = "AppName";
	public final static String SCOPE            = "Scope";
	public final static String DATASTORE        = "datastore.dir";
	/**
	 * InputStream with JSON data
	 */
	public final static String CLIENT_SECRETS   = "client-json-secret";
	
	/**
	 * Full, permissive scope to access all of a user's files, excluding the Application Data folder. Request this scope only when it is strictly necessary.
	 */
	public final static String SCOPE_FULL    = DriveScopes.DRIVE;
	/**
	 * Allows read-only access to file metadata and file content
	 */
	public final static String SCOPE_READONLY= DriveScopes.DRIVE_READONLY;
	/**
	 * Allows access to the Application Data folder
	 * (see https://developers.google.com/drive/v3/web/appdata)
	 */
	public final static String SCOPE_APPDATA = DriveScopes.DRIVE_APPDATA;
	/**
	 * Per-file access to files created or opened by the app. File authorization is granted on a per-user basis and is revoked when the user deauthorizes the app.
	 */
	public final static String SCOPE_FILE    = DriveScopes.DRIVE_FILE;
	/**
	 * Allows read-only access to all photos. The spaces parameter must be set to photos.
	 */
	public final static String SCOPE_PHOTOS_READONLY  = DriveScopes.DRIVE_PHOTOS_READONLY;
	/**
	 * Allows access to Apps Script files
	 */
	public final static String SCOPE_SCRIPTS = DriveScopes.DRIVE_SCRIPTS;
	/**
	 * Allows read-write access to file metadata (excluding downloadUrl and contentHints.thumbnail), but does not allow any access to read, download, write or upload file content. Does not support file creation, trashing or deletion. Also does not allow changing folders or sharing in order to prevent access escalation.
	 */
	public final static String SCOPE_METADATA = DriveScopes.DRIVE_METADATA;
	/**
	 * Allows read-only access to file metadata (excluding downloadUrl and contentHints.thumbnail), but does not allow any access to read or download file content
	 */
	public final static String SCOPE_METADATA_READONLY = DriveScopes.DRIVE_METADATA_READONLY;

}
