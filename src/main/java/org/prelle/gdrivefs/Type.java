package org.prelle.gdrivefs;

public enum Type {
	ROOT,
	FILE_OR_FOLDER,
	SELF_REFERENCE,
	PARENT_REFERENCE
}