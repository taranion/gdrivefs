/**
 * 
 */
package org.prelle.gdrivefs;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SeekableByteChannel;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.api.client.http.ByteArrayContent;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.model.File;

/**
 * @author prelle
 *
 */
public class GoogleDriveUploadChannel implements SeekableByteChannel {
	
	private final static Logger logger = LogManager.getLogger("google.drive");
	
//	private Path tmp; 
	private File internalFile;
	private GoogleDrivePath path;
	private SeekableByteChannel wrapped;

	//-------------------------------------------------------------------
	public GoogleDriveUploadChannel(GoogleDrivePath path, File internalFile) throws IOException {
//		this.path = path;
		this.internalFile = internalFile;
//		tmp = Files.createTempFile(path.getFileName().toString(), "-gdrive");
		logger.debug("  created temp file "+internalFile);
		
//		wrapped = Files.newByteChannel(tmp, StandardOpenOption.WRITE);
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.nio.channels.Channel#isOpen()
	 */
	@Override
	public boolean isOpen() {
		// TODO Auto-generated method stub
		return false;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.nio.channels.Channel#close()
	 */
	@Override
	public void close() throws IOException {
		logger.debug("START: close()");
		try {
//		try {
//			wrapped.close();
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		    System.exit(0);
//		}
		
//		// File's metadata.
//	    File body = new File();
//	    body.setName(path.getFileName().toString());
////	    body.setDescription(description);
//	    body.setMimeType("application/octet-stream");
//
//	    GoogleDrivePath parent = (GoogleDrivePath) path.getParent();
//	    
//	    // Set the parent folder.
//	    if (parent.getFileId()!= null && parent.getFileId().length() > 0) {
//	    	body.setParents(Collections.singletonList(parent.getFileId()));
//	    }
//
//	    // File's content.
//	    FileContent mediaContent = new FileContent("application/octet-stream", tmp.toFile());
//	    File file = ((GoogleDriveFileSystem)path.getFileSystem()).getDrive().files().create(body, mediaContent).execute();
//	    path.setInternalFile(file);
//	    path.setFileId(file.getId());
//	    
//	    Files.delete(tmp);
		} finally {
			logger.debug("STOP : close()");
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.nio.channels.SeekableByteChannel#read(java.nio.ByteBuffer)
	 */
	@Override
	public int read(ByteBuffer dst) throws IOException {
		// TODO Auto-generated method stub
		logger.warn("TODO: read()");
		return 0;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.nio.channels.SeekableByteChannel#write(java.nio.ByteBuffer)
	 */
	@Override
	public int write(ByteBuffer src) throws IOException {
		logger.debug("write("+src+") to "+path+"  (really "+wrapped+")");
		logger.debug("Bytes = "+src.remaining());
		byte[] data = new byte[src.remaining()];
		src.get(data);
		
		Drive driveService = ((GoogleDriveFileSystem)path.getFileSystem()).getDrive();
	    ByteArrayContent mediaContent = new ByteArrayContent("application/octet-stream", data);
	    File file = driveService.files().update(
	    		path.getFileId(), 
	    		null, 
	    		mediaContent).
	    		execute();
		logger.debug("Result "+file+" / "+file.getSize());
		
		file.setSize((long)data.length);
		internalFile.setSize( (long)data.length);
	    
		return data.length;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.nio.channels.SeekableByteChannel#position()
	 */
	@Override
	public long position() throws IOException {
		// TODO Auto-generated method stub
		logger.warn("TODO: position()");
		return 0;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.nio.channels.SeekableByteChannel#position(long)
	 */
	@Override
	public SeekableByteChannel position(long newPosition) throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.nio.channels.SeekableByteChannel#size()
	 */
	@Override
	public long size() throws IOException {
		// TODO Auto-generated method stub
		return 0;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.nio.channels.SeekableByteChannel#truncate(long)
	 */
	@Override
	public SeekableByteChannel truncate(long size) throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

}
