/**
 * 
 */
package org.prelle.gdrivefs;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.api.services.drive.Drive;
import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.FileList;

/**
 * @author prelle
 *
 */
public class GoogleDriveDirectoryStream implements DirectoryStream<Path> {
	
	private final static Logger logger = LogManager.getLogger("google.drive");

	private Drive.Files.List request;
	private GoogleDriveFileSystem fs;
	private GoogleDrivePath directory;
	
	//-------------------------------------------------------------------
	public GoogleDriveDirectoryStream(GoogleDriveFileSystem fs, Drive.Files.List request, GoogleDrivePath dir) {
		this.request = request;
		this.fs = fs;
		this.directory = dir;
		logger.debug("GoogleDriveDirectoryStream<init>");
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.io.Closeable#close()
	 */
	@Override
	public void close() throws IOException {
		// TODO Auto-generated method stub
		
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.nio.file.DirectoryStream#iterator()
	 */
	@Override
	public Iterator<Path> iterator() {
		try {
			return new DriveStreamIterator(fs, request, directory);
		} catch (IOException e) {
			throw new RuntimeException("Failed",e);
		}
	}

}

//-------------------------------------------------------------------
//-------------------------------------------------------------------
class DriveStreamIterator implements Iterator<Path> {
	
	private Logger logger = LogManager.getLogger("google.drive");
	
//	private GoogleDriveFileSystem fs;
	private GoogleDrivePath directory;
	private Drive.Files.List request;
	private List<File> toDeliver;
	private FileList children;

	//-------------------------------------------------------------------
	public DriveStreamIterator(GoogleDriveFileSystem fs, Drive.Files.List request, GoogleDrivePath dir) throws IOException {
		this.request = request;
//		this.fs = fs;
		this.directory = dir;
		toDeliver = new ArrayList<File>();
		
		children = request.execute();
		toDeliver.addAll(children.getFiles());
		logger.debug("DriveStreamIterator<init>: "+toDeliver.size()+" entries");
	}

	//-------------------------------------------------------------------
	@Override
	public boolean hasNext() {
//		logger.debug("  "+toDeliver.size()+" entries left");
		if (!toDeliver.isEmpty()) {
			return true;		
		}
		if (request.getPageToken() != null && request.getPageToken().length() > 0)
			return true;
//		logger.debug("  no next");
		return false;
	}

	//-------------------------------------------------------------------
	@Override
	public Path next() {
		try {
			while (true) {
				if (!toDeliver.isEmpty()) {
					File file = toDeliver.remove(0);
//					logger.debug("next file      = "+file.getTitle());
//					logger.debug("  exp. trashed = "+file.getExplicitlyTrashed());
//					logger.debug("  properties   = "+file.getProperties());
					GoogleDrivePath path = (GoogleDrivePath) directory.resolve(file.getName());
					path.setFileId(file.getId());
					path.setInternalFile(file);
//					path.setParent(directory);
					return path;
				} else {
					request.setPageToken(children.getNextPageToken());
					if (request.getPageToken() != null &&
							request.getPageToken().length() > 0) {
						children = request.execute();
						toDeliver.addAll(children.getFiles());
					} else
						break;
				}
			} ;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
}
