/**
 * 
 */
package org.prelle.gdrivefs;

import java.io.IOException;
import java.net.URI;
import java.nio.file.FileStore;
import java.nio.file.FileSystem;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.nio.file.WatchService;
import java.nio.file.attribute.UserPrincipalLookupService;
import java.nio.file.spi.FileSystemProvider;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.api.services.drive.Drive;
import com.google.api.services.drive.Drive.Files;
import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.FileList;

/**
 * @author prelle
 *
 */
public class GoogleDriveFileSystem extends FileSystem {
	
	private final static Logger logger = LogManager.getLogger("google.drive.fsys");

	private GoogleDriveFileSystemProvider provider;
	private URI uriKey;
	private Drive drive;
	
	private List<FileStore> fileStores;
	private List<Path> rootDirectories;

	//-------------------------------------------------------------------
	GoogleDriveFileSystem(GoogleDriveFileSystemProvider provider, Drive drive, URI uri) {
		this.provider = provider;
		this.uriKey   = uri;
		this.drive = drive;
		fileStores = new ArrayList<>();
		rootDirectories = new ArrayList<>();
		
		fileStores.add(new GoogleDriveFileStore(this, drive));
		rootDirectories.add( new GoogleDrivePath(this));
	}

	//-------------------------------------------------------------------
	Drive getDrive() {
		return drive;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.nio.file.FileSystem#provider()
	 */
	@Override
	public FileSystemProvider provider() {
		return provider;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.nio.file.FileSystem#close()
	 */
	@Override
	public void close() throws IOException {
		logger.info("close() called");
		provider.handleFilesystemClosed(uriKey, this);
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.nio.file.FileSystem#isOpen()
	 */
	@Override
	public boolean isOpen() {
		// TODO Auto-generated method stub
		logger.debug("isOpen()");
		return false;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.nio.file.FileSystem#isReadOnly()
	 */
	@Override
	public boolean isReadOnly() {
		// TODO Auto-generated method stub
		return false;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.nio.file.FileSystem#getSeparator()
	 */
	@Override
	public String getSeparator() {
		// TODO Auto-generated method stub
		logger.debug("isOpen()");
		return "/";
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.nio.file.FileSystem#getRootDirectories()
	 */
	@Override
	public Iterable<Path> getRootDirectories() {
		return rootDirectories;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.nio.file.FileSystem#getFileStores()
	 */
	@Override
	public Iterable<FileStore> getFileStores() {
		return fileStores;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.nio.file.FileSystem#supportedFileAttributeViews()
	 */
	@Override
	public Set<String> supportedFileAttributeViews() {
		// TODO Auto-generated method stub
		logger.warn("TODO: supportedFileAttributeViews()");
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.nio.file.FileSystem#getPath(java.lang.String, java.lang.String[])
	 */
	@Override
	public Path getPath(String first, String... more) {
		logger.debug("getPath()");
		
		PathElement elem = new PathElement(Type.ROOT); 
		elem.setFileId("root");

		List<PathElement> elements = new ArrayList<>();
		elements.add(elem);
		// First
		StringTokenizer tok = new StringTokenizer(first,"/");
		while (tok.hasMoreTokens())
			elements.add(new PathElement(tok.nextToken()));
		// More
		for (String tmp : more)
			elements.add(new PathElement(tmp));
		
		GoogleDrivePath ret = new GoogleDrivePath(this, elements);
		
		return ret;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.nio.file.FileSystem#getPathMatcher(java.lang.String)
	 */
	@Override
	public PathMatcher getPathMatcher(String syntaxAndPattern) {
		// TODO Auto-generated method stub
		logger.warn("TODO: getPathMatcher()");
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.nio.file.FileSystem#getUserPrincipalLookupService()
	 */
	@Override
	public UserPrincipalLookupService getUserPrincipalLookupService() {
		// TODO Auto-generated method stub
		logger.warn("TODO: getUserPrincipalLookupService()");
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.nio.file.FileSystem#newWatchService()
	 */
	@Override
	public WatchService newWatchService() throws IOException {
		// TODO Auto-generated method stub
		logger.warn("TODO: newWatchService()");
		return new GoogleDriveWatchService();
	}

	//-------------------------------------------------------------------
	/**
	 * @return TRUE, if all elements could be bound
	 */
	boolean bindPathBest(GoogleDrivePath path) throws IOException {
		logger.debug("START: bindPathBest("+path+")");
		try {
			if (!path.isAbsolute()) {
				throw new IllegalArgumentException("Not absolute");
			}

			PathElement lastOK = null;
			outer:
				for (PathElement tmp : path.getElements()) {
					int pos = path.getElements().indexOf(tmp);
					logger.trace("  index is "+pos+" = "+tmp);
					if (tmp.getFileId()!=null) {
						lastOK = tmp;
						continue outer;
					} else {
						String needle = tmp.name;
						logger.debug("  search "+needle+" in children of "+path.getElements().subList(0, pos));
						Files.List request = drive.files().list();
						String q1 = String.format("'%s' in parents", lastOK.getFileId());
						String q2 = "trashed=false";
						request.setQ(q1+" and "+q2);
						FileList list = request.execute();
						List<File> items = list.getFiles();
						inner:
							for (File tmp2 : items) {
								if (tmp2.getName().equals(needle)) {
									logger.debug("    Found. Set "+tmp+" to "+tmp2.getId());
									tmp.setFileId(tmp2.getId());
									tmp.setInternalFile(tmp2);
									lastOK = tmp;
									logger.debug("  LastOK now "+path.getElements().subList(0, pos+1));
									break inner;
								}
							}

						if (tmp.getFileId()==null) {
							logger.debug("Best binding effort of "+path+" is "+lastOK);
							logger.debug("STOP : bindPathBest("+path+") ... result is "+path.dumpLine());
							return false;
						}
					}

				}
		
//		GoogleDrivePath lastOK = null;
//		for (Iterator<Path> it=path.iterator(); it.hasNext(); ) {
//			GoogleDrivePath tmp = (GoogleDrivePath) it.next();
//			String needle = tmp.getFileName().toString();
//			if (tmp.getFileId()!=null) {
//				if (lastOK==null)
//					lastOK = tmp;
//				else {
//					lastOK = (GoogleDrivePath) lastOK.resolve(tmp);
//					lastOK.setFileId(tmp.getFileId());
//					lastOK.setInternalFile(tmp.getInternalFile());
//				}
//			} else {
//				logger.debug("search "+needle+" in children of "+lastOK.dumpLine());
//				Files.List request = drive.files().list();
//				String q1 = String.format("'%s' in parents", lastOK.getFileId());
//				String q2 = "trashed=false";
//				request.setQ(q1+" and "+q2);
//				FileList list = request.execute();
//				List<File> items = list.getItems();
//				for (File tmp2 : items) {
//					logger.debug("* "+tmp2.getTitle());
//					if (tmp2.getTitle().equals(needle)) {
//						logger.debug("Found. Set "+path+" to "+tmp2.getId());
////						tmp.setFileId(tmp2.getId());
////						tmp.setInternalFile(tmp2);
////						logger.debug("Path now "+tmp.dumpLine());
//						lastOK = (GoogleDrivePath) lastOK.resolve(needle);
//						lastOK.setFileId(tmp2.getId());
//						lastOK.setInternalFile(tmp2);
//						logger.debug("LastOK now "+lastOK.dumpLine());
//						break;
//					}
//				}
//
//			}
//			
//			if (tmp.getFileId()==null) {
//				logger.debug("Best binding effort of "+path+" is "+lastOK);
//				logger.debug("STOP : bindPathBest("+path+") ... result is "+path.dumpLine());
//				return false;
//			}
//		}

//		logger.debug("STOP : bindPathBest("+path+")");
		return true;
		} finally {
			logger.debug("STOP : bindPathBest("+path+")");
		}
	}

	//-------------------------------------------------------------------
	void bindPath(GoogleDrivePath path) throws IOException {
		bindPathBest(path);
		if ( ((GoogleDrivePath)path).getFileId()==null)
			throw new IOException("Unbindable");		
	}

	//-------------------------------------------------------------------
	int getBindLength(GoogleDrivePath path) throws IOException {
		bindPathBest(path);
		int len = 0;
		for (int i=0; i<path.getElements().size(); i++) {
			if (path.getInternalFile()==null)
				break;
			len++;
		}
		return len;
	}

}
