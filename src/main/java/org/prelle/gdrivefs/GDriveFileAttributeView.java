/**
 * 
 */
package org.prelle.gdrivefs;

import java.io.IOException;
import java.nio.file.attribute.FileAttributeView;

/**
 * @author spr
 *
 */
public interface GDriveFileAttributeView extends FileAttributeView {

	//-------------------------------------------------------------------
	public String getDescription();
	public void setDescription(String value) throws IOException;

	//-------------------------------------------------------------------
	public String getMimeType();
	public void setMimeType(String value) throws IOException ;
	
}
