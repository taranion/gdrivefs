/**
 * 
 */
package org.prelle.gdrivefs;

import java.io.IOException;
import java.nio.file.attribute.BasicFileAttributeView;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.api.client.util.DateTime;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.Drive.Files;
import com.google.api.services.drive.model.File;

/**
 * @author prelle
 *
 */
class GoogleDriveBasicFileAttributeView implements BasicFileAttributeView, GDriveFileAttributeView {
	
	private final static Logger logger = LogManager.getLogger("google.drive.prov");
	
	private File file;
	private Drive service;

	//-------------------------------------------------------------------
	public GoogleDriveBasicFileAttributeView(File file, Drive drive) {
		this.file = file;
		this.service = drive;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.nio.file.attribute.AttributeView#name()
	 */
	@Override
	public String name() {
		return file.getName();
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.nio.file.attribute.BasicFileAttributeView#readAttributes()
	 */
	@Override
	public BasicFileAttributes readAttributes() throws IOException {
		// TODO Auto-generated method stub
		logger.warn("TODO: readAttributes()");
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.nio.file.attribute.BasicFileAttributeView#setTimes(java.nio.file.attribute.FileTime, java.nio.file.attribute.FileTime, java.nio.file.attribute.FileTime)
	 */
	@Override
	public void setTimes(FileTime lastModifiedTime, FileTime lastAccessTime, FileTime createTime) throws IOException {
		logger.debug("setTimes("+lastModifiedTime+", "+lastAccessTime+", "+createTime+")");
		
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX");
		
		Files.Update request = service.files().update(file.getId(), null);
//		request.setModifiedDateBehavior("fromBody");

		if (lastAccessTime!=null) {
			logger.warn("TODO  access = "+format.format(new Date(lastAccessTime.toMillis())));
			file.setViewedByMeTime(new DateTime(lastAccessTime.toMillis()));
//			request.setUpdateViewedDate(true);
		}
		
		if (lastModifiedTime!=null) {
			logger.warn("TODO  modify = "+format.format(new Date(lastModifiedTime.toMillis())));
			file.setModifiedTime(new DateTime(lastModifiedTime.toMillis()));
//			request.setSetModifiedDate(true);			
		}
		
		request.execute();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.gdrivefs.GDriveFileAttributeView#getDescription()
	 */
	@Override
	public String getDescription() {
		try {
			File result = service.files().get(file.getId()).setFields("description").execute();
			return result.getDescription();
		} catch (IOException e) {
			logger.error("Cannot obtain description",e);
		}
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.gdrivefs.GDriveFileAttributeView#setDescription(java.lang.String)
	 */
	@Override
	public void setDescription(String value) throws IOException {
		Files.Update request = service.files().update(file.getId(), null);
		request.setFields("description");
		file.set("description", value);
		request.execute();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.gdrivefs.GDriveFileAttributeView#getMimeType()
	 */
	@Override
	public String getMimeType() {
		try {
			File result = service.files().get(file.getId()).setFields("mimeType").execute();
			return result.getDescription();
		} catch (IOException e) {
			logger.error("Cannot obtain description",e);
		}
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.gdrivefs.GDriveFileAttributeView#setMimeType(java.lang.String)
	 */
	@Override
	public void setMimeType(String value) throws IOException {
		Files.Update request = service.files().update(file.getId(), null);
		request.setFields("mimeType");
		file.set("mimeType", value);
		request.execute();
	}

}
