/**
 * 
 */
package org.prelle.gdrivefs;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.nio.channels.SeekableByteChannel;
import java.nio.file.AccessMode;
import java.nio.file.CopyOption;
import java.nio.file.DirectoryNotEmptyException;
import java.nio.file.DirectoryStream;
import java.nio.file.DirectoryStream.Filter;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.FileStore;
import java.nio.file.FileSystem;
import java.nio.file.FileSystemAlreadyExistsException;
import java.nio.file.LinkOption;
import java.nio.file.NoSuchFileException;
import java.nio.file.OpenOption;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributeView;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileAttribute;
import java.nio.file.attribute.FileAttributeView;
import java.nio.file.spi.FileSystemProvider;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.DataStoreFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.Drive.Files;
import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.FileList;
//import com.google.api.services.drive.model.ParentReference;

/**
 * @author prelle
 *
 */
public class GoogleDriveFileSystemProvider extends FileSystemProvider {
	
	private final static Logger logger = LogManager.getLogger("google.drive.prov");

	public final static String SCHEME = "google";
	
	/**
	 * Be sure to specify the name of your application. If the application name is {@code null} or
	 * blank, the application will log a warning. Suggested format is "MyCompany-ProductName/1.0".
	 */
//	private static final String APPLICATION_NAME = "RPGFramework/Babylon/1.0";

	
	/** Directory to store user credentials. */
	private static final String DATA_STORE_DIR = System.getProperty("user.home") + System.getProperty("file.separator")+ ".store/drive_sample";

	/**
	 * Global instance of the {@link DataStoreFactory}. The best practice is to make it a single
	 * globally shared instance across your application.
	 */
	private FileDataStoreFactory dataStoreFactory;

	/** Global instance of the HTTP transport. */
	private HttpTransport httpTransport;

	/** Global instance of the JSON factory. */
	private final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
	
	private Map<URI, FileSystem> fsByURI;

	//-------------------------------------------------------------------
	public GoogleDriveFileSystemProvider() {
		logger.info("Initiated");
		fsByURI = new HashMap<>();
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.nio.file.spi.FileSystemProvider#getScheme()
	 */
	@Override
	public String getScheme() {
		return SCHEME;
	}

	//-------------------------------------------------------------------
	/** 
	 * Authorizes the installed application to access user's protected data. 
	 */
	private Credential authorize(String scope, InputStream secrets) throws Exception {
		// load client secrets
		GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(JSON_FACTORY,
				new InputStreamReader(secrets));
		// set up authorization code flow
		GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(
				httpTransport, JSON_FACTORY, clientSecrets,
				Collections.singleton(scope)).setDataStoreFactory(dataStoreFactory)
				.build();
		// authorize
		return new AuthorizationCodeInstalledApp(flow, new LocalServerReceiver()).authorize("user");
	}

	//-------------------------------------------------------------------
	void handleFilesystemClosed(URI key, GoogleDriveFileSystem fs) {
		logger.debug("filesystem closed");
		fsByURI.remove(key);
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.nio.file.spi.FileSystemProvider#newFileSystem(java.net.URI, java.util.Map)
	 */
	@Override
	public FileSystem newFileSystem(URI uri, Map<String, ?> env) throws IOException {
		logger.info("newFileSystem("+uri+", "+env+")");

		// Check if filesystem already exists
		if (fsByURI.containsKey(uri))
			throw new FileSystemAlreadyExistsException();
		
		// Ensure Application name is set
		if (!env.containsKey(GoogleDriveFSConstants.APPLICATION_NAME))
			throw new IOException("Missing property "+GoogleDriveFSConstants.APPLICATION_NAME);
		String appName = (String)env.get(GoogleDriveFSConstants.APPLICATION_NAME);
		
		// Ensure Scope property is set
		if (!env.containsKey(GoogleDriveFSConstants.SCOPE))
			throw new IOException("Missing property "+GoogleDriveFSConstants.SCOPE);
		String scope = (String)env.get(GoogleDriveFSConstants.SCOPE);
		
		// Ensure Scope property is set
		if (!env.containsKey(GoogleDriveFSConstants.CLIENT_SECRETS))
			throw new IOException("Missing property "+GoogleDriveFSConstants.CLIENT_SECRETS);
		InputStream secrets = (InputStream)env.get(GoogleDriveFSConstants.CLIENT_SECRETS);
		
		
		java.io.File dataStoreDir = new java.io.File( (env.containsKey(GoogleDriveFSConstants.DATASTORE)?env.get(GoogleDriveFSConstants.DATASTORE).toString():DATA_STORE_DIR) );
		try {
			httpTransport = GoogleNetHttpTransport.newTrustedTransport();
			dataStoreFactory = new FileDataStoreFactory(dataStoreDir);
			// authorization
			Credential credential = authorize(scope, secrets);
			// set up the global Drive instance
			Drive drive = new Drive.Builder(httpTransport, JSON_FACTORY, credential).setApplicationName(
					appName).build();

			// run commands
			GoogleDriveFileSystem fs = new GoogleDriveFileSystem(this, drive, uri);
			fsByURI.put(uri, fs);
			logger.debug("Filesystem "+uri+" created");
			return fs;
		} catch (IOException e) {
			throw e;
		} catch (Throwable t) {
			logger.fatal("Failed creating filesystem '"+uri+"': "+t,t);
			throw new IOException("Failed creating filesystem '"+uri+"'", t);
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.nio.file.spi.FileSystemProvider#getFileSystem(java.net.URI)
	 */
	@Override
	public FileSystem getFileSystem(URI uri) {
		// TODO Auto-generated method stub
		logger.info("getFileSystem("+uri+")");
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.nio.file.spi.FileSystemProvider#getPath(java.net.URI)
	 */
	@Override
	public Path getPath(URI longURI) {
		// TODO Auto-generated method stub
		String uri = longURI.toString().substring(getScheme().length()+1); 
		if (uri.startsWith("//"))
			uri = uri.substring(2);
		
		logger.info("getPath("+uri+")  look in ");
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.nio.file.spi.FileSystemProvider#newByteChannel(java.nio.file.Path, java.util.Set, java.nio.file.attribute.FileAttribute[])
	 */
	@Override
	public SeekableByteChannel newByteChannel(Path path, Set<? extends OpenOption> options, FileAttribute<?>... attrs)
			throws IOException {
		logger.debug("START: newByteChannel("+path+", "+options+", "+Arrays.toString(attrs)+")");
		
		try {
		GoogleDrivePath realPath = (GoogleDrivePath)path;
//		GoogleDrivePath   parent = (GoogleDrivePath)path.getParent();
		
		DriveUtil.createFile(realPath);
		
		return new GoogleDriveUploadChannel(realPath, realPath.getInternalFile());
		
//		// File's metadata.
//	    File body = new File();
//	    body.setTitle(path.getFileName().toString());
////	    body.setDescription(description);
//	    body.setMimeType("application/octet-stream");
//
//	    // Set the parent folder.
//	    if (parent.getFileId()!= null && parent.getFileId().length() > 0) {
//	      body.setParents(
//	          Arrays.asList(new ParentReference().setId(parent.getFileId())));
//	    }
//
//	    // File's content.
//	    java.io.File fileContent = new java.io.File(path.getFileName().toString());
//	    FileContent mediaContent = new FileContent("application/octet-stream", fileContent);
//	    try {
//	      File file = ((GoogleDriveFileSystem)realPath.getFileSystem()).getDrive().files().insert(body, mediaContent).execute();
//	      return file;
//	    } catch (IOException e) {
//	      System.out.println("An error occured: " + e);
//	      return null;
//	    }
//
//		return null;
		} finally {
			logger.debug("STOP : newByteChannel()");
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.nio.file.spi.FileSystemProvider#newDirectoryStream(java.nio.file.Path, java.nio.file.DirectoryStream.Filter)
	 */
	@Override
	public DirectoryStream<Path> newDirectoryStream(Path dir, Filter<? super Path> filter) throws IOException {
		logger.info("newDirectoryStream("+dir+", "+filter+")");
		
		GoogleDriveFileSystem fs = (GoogleDriveFileSystem) dir.getFileSystem();
		GoogleDrivePath realDir  = (GoogleDrivePath)dir;
		
		Files.List request = fs.getDrive().files().list();
		String q1 = String.format("'%s' in parents", realDir.getFileId());
		String q2 = "trashed=false";
		request.setQ(q1+" and "+q2);
//		request.setQ("parents in '"+realDir.getFileId()+"'");
//		request.setQ("trashed = false");
		
		return new GoogleDriveDirectoryStream(fs, request, realDir);
	}

//	//-------------------------------------------------------------------
//	File getFile(GoogleDriveFileSystem fs, String dir) {
//		File file = new File();
//		Drive.Files.Insert request = fs.getDrive().files().insert(dir);
//		File file = request.execute();
//		
//	}

	//-------------------------------------------------------------------
	/**
	 * @see java.nio.file.spi.FileSystemProvider#createDirectory(java.nio.file.Path, java.nio.file.attribute.FileAttribute[])
	 */
	@Override
	public void createDirectory(Path dir, FileAttribute<?>... attrs) throws IOException {
		// TODO Auto-generated method stub
		logger.debug("START: **********************createDirectory("+dir+", "+Arrays.toString(attrs)+")");
		
		GoogleDriveFileSystem fs = (GoogleDriveFileSystem) dir.getFileSystem();
		GoogleDrivePath realDir  = (GoogleDrivePath)dir;
		logger.debug("  real="+realDir.dumpLine());
		if (!dir.isAbsolute()) {
			logger.debug("STOP : createDirectory("+dir+", "+Arrays.toString(attrs)+")");
			throw new NoSuchFileException("Can only create absolute pathes");
		}
		
		if (realDir.getFileId()!=null) {
			logger.debug("STOP : createDirectory("+dir+", "+Arrays.toString(attrs)+")");
			throw new FileAlreadyExistsException(realDir.toString());
		}
		
		
		GoogleDrivePath parent = (GoogleDrivePath)realDir.getParent();
		boolean allBound = ((GoogleDriveFileSystem)parent.getFileSystem()).bindPathBest(parent);
		if (!allBound) {
			logger.warn("Parent of directory to create could not be bound: "+parent);
			logger.debug("STOP : createDirectory("+dir+", "+Arrays.toString(attrs)+")");
			throw new NoSuchFileException(parent.toString());
		}
		
		logger.info("**********************Create directory '"+dir.getFileName().toString()+"' under "+parent);
		if (dir.getFileName().toString().isEmpty())
			throw new IllegalArgumentException("Filename is empty: "+realDir.dumpLine());
		
		File newDir = new File();
		newDir.setName(dir.getFileName().toString());
//		newDir.setDescription("Neuer Ordner");
		newDir.setMimeType("application/vnd.google-apps.folder");
		if (parent.getFileId()!=null && !parent.getFileId().isEmpty())
			newDir.setParents(Collections.singletonList(parent.getFileId()));
		
		logger.debug("Insert file "+dir.getFileName()+" as child of "+parent+" // "+parent.getFileId());
		if (parent.getFileId()==null && String.valueOf(parent).length()>1) {
			logger.warn("FileId of parent unknown.\nrealDir="+realDir.dumpLine());
			logger.debug("STOP : createDirectory("+dir+", "+Arrays.toString(attrs)+")");
			throw new NoSuchFileException(parent.toString());
		}
		
		Drive.Files.Create request = fs.getDrive().files().create(newDir);
		File file = request.execute();
		logger.debug("Created "+realDir+" with ID "+file.getId());
		realDir.setFileId(file.getId());
		realDir.setInternalFile(file);
		logger.debug("STOP : createDirectory("+dir+", "+Arrays.toString(attrs)+")");
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.nio.file.spi.FileSystemProvider#delete(java.nio.file.Path)
	 */
	@Override
	public void delete(Path path) throws IOException {
		// TODO Auto-generated method stub
		logger.warn("TODO: delete("+path+")");

		GoogleDrivePath realPath = (GoogleDrivePath)path;
		GoogleDriveFileSystem fs = (GoogleDriveFileSystem) realPath.getFileSystem();
		
		/*
		 * TODO: If the path is not bound yet, to it
		 */
		fs.bindPathBest(realPath);
		if (realPath.getFileId()==null) {
			logger.warn("No fileId for "+path);
			throw new NoSuchFileException(path.toString());
		}
		
		
		if (java.nio.file.Files.isDirectory(path)) {
			Files.List request = ((GoogleDriveFileSystem)realPath.getFileSystem()).getDrive().files().list();
			String q1 = String.format("'%s' in parents", realPath.getFileId());
			String q2 = "trashed=false";
			request.setQ(q1+" and "+q2);
			FileList list = request.execute();
			List<File> items = list.getFiles();
			if (!items.isEmpty()) {
				logger.debug("directory "+realPath+" cannot be deleted - not empty");
				throw new DirectoryNotEmptyException(path.toString());
			}
			
			// Directory empty ... can be deleted
			logger.info("Delete directory "+path+" ... DriveID = "+realPath.getFileId());
			Files.Delete delete = ((GoogleDriveFileSystem)realPath.getFileSystem()).getDrive().files().delete(realPath.getFileId());
			delete.execute();
			return;
		} else if (java.nio.file.Files.isRegularFile(path)) {
			logger.info("Delete regular file "+path+" ... DriveID = "+realPath.getFileId());
			Files.Delete request = ((GoogleDriveFileSystem)realPath.getFileSystem()).getDrive().files().delete(realPath.getFileId());
			request.execute();
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.nio.file.spi.FileSystemProvider#copy(java.nio.file.Path, java.nio.file.Path, java.nio.file.CopyOption[])
	 */
	@Override
	public void copy(Path source, Path target, CopyOption... options) throws IOException {
		// TODO Auto-generated method stub
		logger.warn("TODO: copy");
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.nio.file.spi.FileSystemProvider#move(java.nio.file.Path, java.nio.file.Path, java.nio.file.CopyOption[])
	 */
	@Override
	public void move(Path source, Path target, CopyOption... options) throws IOException {
		// TODO Auto-generated method stub
		logger.warn("TODO: move");
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.nio.file.spi.FileSystemProvider#isSameFile(java.nio.file.Path, java.nio.file.Path)
	 */
	@Override
	public boolean isSameFile(Path path, Path path2) throws IOException {
		// TODO Auto-generated method stub
		logger.warn("TODO: isSameFile");
		return false;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.nio.file.spi.FileSystemProvider#isHidden(java.nio.file.Path)
	 */
	@Override
	public boolean isHidden(Path path) throws IOException {
		// TODO Auto-generated method stub
		logger.warn("TODO: isHidden");
		return false;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.nio.file.spi.FileSystemProvider#getFileStore(java.nio.file.Path)
	 */
	@Override
	public FileStore getFileStore(Path path) throws IOException {
		// TODO Auto-generated method stub
		logger.warn("TODO: isHidden");
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.nio.file.spi.FileSystemProvider#checkAccess(java.nio.file.Path, java.nio.file.AccessMode[])
	 */
	@Override
	public void checkAccess(Path path, AccessMode... modes) throws IOException {
		GoogleDrivePath realPath = (GoogleDrivePath)path;
		((GoogleDriveFileSystem)path.getFileSystem()).bindPathBest(realPath);
		if (realPath.getFileId()==null)
			throw new NoSuchFileException(path.toString());

//		logger.warn("TODO: checkAccess "+path+" ... allow for now");
		
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.nio.file.spi.FileSystemProvider#getFileAttributeView(java.nio.file.Path, java.lang.Class, java.nio.file.LinkOption[])
	 */
	@SuppressWarnings("unchecked")
	@Override
	public <V extends FileAttributeView> V getFileAttributeView(Path path, Class<V> type, LinkOption... options) {
		logger.debug("getFileAttributeView("+type+")");
		GoogleDrivePath realPath = (GoogleDrivePath)path;
		
		if (type==BasicFileAttributeView.class || type==GDriveFileAttributeView.class) {
			return (V) new GoogleDriveBasicFileAttributeView(realPath.getInternalFile(), ((GoogleDriveFileSystem)realPath.getFileSystem()).getDrive());
		}
		logger.warn("TODO: getFileAttributeView("+path+", "+type+")");
		logger.error("Don't know how to instantiate "+type);
		throw new RuntimeException("Unsupported FileAttributeView type "+String.valueOf(type));
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.nio.file.spi.FileSystemProvider#readAttributes(java.nio.file.Path, java.lang.Class, java.nio.file.LinkOption[])
	 */
	@SuppressWarnings("unchecked")
	@Override
	public <A extends BasicFileAttributes> A readAttributes(Path path, Class<A> type, LinkOption... options)
			throws IOException {
		logger.debug("readAttributes("+path+", "+type+", "+Arrays.toString(options)+")");
		
		GoogleDrivePath realPath = (GoogleDrivePath)path;
		if (realPath.getFileId()==null) {
//			logger.debug("Needs binding");
			((GoogleDriveFileSystem)path.getFileSystem()).bindPathBest(realPath);
			if (realPath.getFileId()==null)
				throw new NoSuchFileException(path.toString());
		} 
		if (realPath.getInternalFile()==null && realPath.getFileId()==null) {
			logger.error("Neither fileId or internal file for "+path);
			System.exit(0);
		}
		if (realPath.getInternalFile()==null) {
			logger.error("No internal file set, but element seems known "+realPath.dumpLine());
			throw new NullPointerException("No internal file, but fileId: "+path);
		}
			
		logger.debug("read from "+realPath.getInternalFile().getId()+" vs "+realPath.getFileId());
		
		logger.debug("meta = "+realPath.getInternalFile()); 
		
		return (A)new GoogleDriveFileAttributes( ((GoogleDrivePath)path).getInternalFile() );
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.nio.file.spi.FileSystemProvider#readAttributes(java.nio.file.Path, java.lang.String, java.nio.file.LinkOption[])
	 */
	@Override
	public Map<String, Object> readAttributes(Path path, String attributes, LinkOption... options) throws IOException {
		// TODO Auto-generated method stub
		logger.warn("TODO: readAttributes("+path+")");
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.nio.file.spi.FileSystemProvider#setAttribute(java.nio.file.Path, java.lang.String, java.lang.Object, java.nio.file.LinkOption[])
	 */
	@Override
	public void setAttribute(Path path, String attribute, Object value, LinkOption... options) throws IOException {
		// TODO Auto-generated method stub
		logger.warn("TODO: setAttribute("+path+")");

	}

}
