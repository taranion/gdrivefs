/**
 * 
 */
package org.prelle.gdrivefs;

import java.io.IOException;
import java.io.InputStreamReader;
import java.security.GeneralSecurityException;
import java.util.Collections;
import java.util.ResourceBundle;
import java.util.Timer;
import java.util.TimerTask;
import java.util.prefs.Preferences;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.DataStoreFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.DriveScopes;

/**
 * @author prelle
 *
 */
public class GoogleDriveProvider {
	
	private final static Logger logger = LogManager.getLogger("google.drive");
	
	protected final static ResourceBundle RES = ResourceBundle.getBundle("i18n/google");

	public final static String DATASTORE_DIR=System.getProperty("user.home")+"/RPGFramework";
	public final static String PROP_DATASTORE_DIR = "google.drive.datastore.dir";

	/**
	 * Be sure to specify the name of your application. If the application name is {@code null} or
	 * blank, the application will log a warning. Suggested format is "MyCompany-ProductName/1.0".
	 */
	private static final String APPLICATION_NAME = "RPGFramework/Babylon/1.0";
	/** Global instance of the JSON factory. */
	private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
	/**
	 * Global instance of the {@link DataStoreFactory}. The best practice is to make it a single
	 * globally shared instance across your application.
	 */
	private static FileDataStoreFactory dataStoreFactory;

	/** Global instance of the HTTP transport. */
	private static HttpTransport httpTransport;

	
	private static Preferences PREF = Preferences.userRoot().node("/org/prelle/rpgframework/online/social/google");
	
	
	private Timer timer;
	
	private Credential credential;
	private Drive drive;
	
	//------------------------------------------------------------------
	public GoogleDriveProvider() {
		logger.debug("initialize");
//		datastoreDir = new ConfigOptionImpl(PROP_DATASTORE_DIR, ConfigOption.Type.TEXT, DATASTORE_DIR, RES.getString("option."+PROP_DATASTORE_DIR));
//		pass = new ConfigOptionImpl(PROP_COSPACE_PASS, ConfigOption.Type.PASSWORD, null);
//		host = new ConfigOptionImpl(PROP_COSPACE_SERVER, ConfigOption.Type.TEXT, "api.cospace.de");
//		port = new ConfigOptionImpl(PROP_COSPACE_PORT, ConfigOption.Type.NUMBER, 80);
		
		if (timer==null) {
			timer = new Timer("GoogleReconnect", false);
			timer.scheduleAtFixedRate(new TimerTask(){
				@Override
				public void run() {
					reconnectCheck();
				}}, 0, 60000);
		}
		
	}

	//-----------------------------------------------------------------
	private void reconnectCheck() {
		if (drive==null) {
			logger.debug("Try connecting to Google");
			try {
				login();
			} catch (IOException e) {
				logger.error("Failed logging in: "+e,e);
			}
		}
	}

	//-----------------------------------------------------------------
	private boolean login() throws IOException {
		logger.debug("login");

		// Set default values
		if (PREF.get(PROP_DATASTORE_DIR, null)==null)
			PREF.put(PROP_DATASTORE_DIR, DATASTORE_DIR);

		try {
			httpTransport = GoogleNetHttpTransport.newTrustedTransport();
		} catch (GeneralSecurityException e) {
			logger.error("Failed preparing HTTP transport",e);
			throw new IOException("Failed preparing HTTP transport", e);
		}
		
		java.io.File DATA_STORE_DIR = new java.io.File(PREF.get(PROP_DATASTORE_DIR, DATASTORE_DIR));
		dataStoreFactory = new FileDataStoreFactory(DATA_STORE_DIR);
		GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(JSON_FACTORY,
				new InputStreamReader(ClassLoader.getSystemResourceAsStream("client_secrets.json")));
		//		GoogleClientSecrets clientSecrets = new GoogleClientSecrets();
		//		clientSecrets.set("client_id", "sigiswild@gmail.com");
		//		clientSecrets.set("client_secret", "Tamina88Ke");
		// set up authorization code flow
		GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(
				httpTransport, JSON_FACTORY, clientSecrets,
				Collections.singleton(DriveScopes.DRIVE_FILE)).setDataStoreFactory(dataStoreFactory)
				.build();
		// authorize
		credential = new AuthorizationCodeInstalledApp(flow, new LocalServerReceiver()).authorize("user");
		logger.debug("Loaded credentials");
		drive = new Drive.Builder(httpTransport, JSON_FACTORY, credential).setApplicationName(
				APPLICATION_NAME).build();
		logger.info("Successfully logged in to Google Drive");
		
		return true;
	}

}
