/**
 * 
 */
package org.prelle.gdrivefs;

import java.io.IOException;
import java.nio.file.FileStore;
import java.nio.file.attribute.FileAttributeView;
import java.nio.file.attribute.FileStoreAttributeView;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.api.services.drive.Drive;
import com.google.api.services.drive.model.About;

/**
 * @author prelle
 *
 */
public class GoogleDriveFileStore extends FileStore {
	
	private final static Logger logger = LogManager.getLogger("google.drive");
	
	private GoogleDriveFileSystem filesystem;
//	private Drive drive;

	//-------------------------------------------------------------------
	public GoogleDriveFileStore(GoogleDriveFileSystem fs, Drive drive) {
//		this.drive = drive;
		this.filesystem = fs;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.nio.file.FileStore#name()
	 */
	@Override
	public String name() {
		return "/";
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.nio.file.FileStore#type()
	 */
	@Override
	public String type() {
		return GoogleDriveFileSystemProvider.SCHEME;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.nio.file.FileStore#isReadOnly()
	 */
	@Override
	public boolean isReadOnly() {
		// TODO Auto-generated method stub
		logger.warn("TODO: isReadOnly");
		return false;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.nio.file.FileStore#getTotalSpace()
	 */
	@Override
	public long getTotalSpace() throws IOException {
		About about = filesystem.getDrive().about().get().setFields("storageQuota").execute();
		return about.getStorageQuota().getLimit();
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.nio.file.FileStore#getUsableSpace()
	 */
	@Override
	public long getUsableSpace() throws IOException {
		About about = filesystem.getDrive().about().get().setFields("storageQuota").execute();
		return about.getStorageQuota().getLimit() - about.getStorageQuota().getUsage();
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.nio.file.FileStore#getUnallocatedSpace()
	 */
	@Override
	public long getUnallocatedSpace() throws IOException {
		About about = filesystem.getDrive().about().get().setFields("storageQuota").execute();
		return about.getStorageQuota().getUsage();
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.nio.file.FileStore#supportsFileAttributeView(java.lang.Class)
	 */
	@Override
	public boolean supportsFileAttributeView(Class<? extends FileAttributeView> type) {
		// TODO Auto-generated method stub
		logger.warn("TODO: supportsFileAttributeView");
		return false;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.nio.file.FileStore#supportsFileAttributeView(java.lang.String)
	 */
	@Override
	public boolean supportsFileAttributeView(String name) {
		// TODO Auto-generated method stub
		logger.warn("TODO: supportsFileAttributeView");
		return false;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.nio.file.FileStore#getFileStoreAttributeView(java.lang.Class)
	 */
	@Override
	public <V extends FileStoreAttributeView> V getFileStoreAttributeView(Class<V> type) {
		// TODO Auto-generated method stub
		logger.warn("TODO: FileStoreAttributeView");
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.nio.file.FileStore#getAttribute(java.lang.String)
	 */
	@Override
	public Object getAttribute(String attribute) throws IOException {
		// TODO Auto-generated method stub
		logger.warn("TODO: getAttribute("+attribute+")");
		return null;
	}

}
