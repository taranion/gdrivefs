/**
 * 
 */
package org.prelle.gdrivefs;

import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.api.client.util.DateTime;
import com.google.api.services.drive.model.File;

/**
 * @author prelle
 *
 */
public class GoogleDriveFileAttributes implements BasicFileAttributes {
	
	private final static Logger logger = LogManager.getLogger("google.drive");

	private File file;
	
	//-------------------------------------------------------------------
	public GoogleDriveFileAttributes(File file) {
		logger.debug("GoogleDriveFileAttributes.<init>("+file+")");
		if (file==null)
			throw new NullPointerException();
		this.file = file;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.nio.file.attribute.BasicFileAttributes#lastModifiedTime()
	 */
	@Override
	public FileTime lastModifiedTime() {
		DateTime stamp = file.getModifiedTime();
		if (stamp==null)
			stamp = file.getCreatedTime();
		long millis = stamp.getValue();
		return FileTime.fromMillis( millis );
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.nio.file.attribute.BasicFileAttributes#lastAccessTime()
	 */
	@Override
	public FileTime lastAccessTime() {
		return FileTime.fromMillis( file.getViewedByMeTime().getValue() );
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.nio.file.attribute.BasicFileAttributes#creationTime()
	 */
	@Override
	public FileTime creationTime() {
		return FileTime.fromMillis( file.getCreatedTime().getValue() );
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.nio.file.attribute.BasicFileAttributes#isRegularFile()
	 */
	@Override
	public boolean isRegularFile() {
		// TODO Auto-generated method stub
		logger.debug("isRegularFile "+file.getMimeType());
		return !"application/vnd.google-apps.folder".equals(file.getMimeType());
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.nio.file.attribute.BasicFileAttributes#isDirectory()
	 */
	@Override
	public boolean isDirectory() {
		return "application/vnd.google-apps.folder".equals(file.getMimeType());
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.nio.file.attribute.BasicFileAttributes#isSymbolicLink()
	 */
	@Override
	public boolean isSymbolicLink() {
		return false;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.nio.file.attribute.BasicFileAttributes#isOther()
	 */
	@Override
	public boolean isOther() {
		// TODO Auto-generated method stub
		return false;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.nio.file.attribute.BasicFileAttributes#size()
	 */
	@Override
	public long size() {
		logger.debug("size() on "+file);
		Long result = file.getSize();
		logger.debug("  returned "+result);
		if (result==null) {
			logger.error("file.getSize() returned NULL on "+file);
			return 0;
		}
		return file.getSize();
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.nio.file.attribute.BasicFileAttributes#fileKey()
	 */
	@Override
	public Object fileKey() {
//		logger.warn("TODO: fileKey "+file);
//		// TODO Auto-generated method stub
		return file;
	}

}
