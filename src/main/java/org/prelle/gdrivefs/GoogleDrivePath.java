/**
 * 
 */
package org.prelle.gdrivefs;

import java.io.IOException;
import java.net.URI;
import java.nio.file.FileSystem;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.WatchEvent.Kind;
import java.nio.file.WatchEvent.Modifier;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.api.services.drive.model.File;

/**
 * @author prelle
 *
 */
public class GoogleDrivePath implements Path {
	
	private final static Logger logger = LogManager.getLogger("google.drive.path");
	
	private GoogleDriveFileSystem filesystem;
	
	private List<PathElement> elements;
//	private Map<PathElement, GoogleDrivePath> children;
//	private GoogleDrivePath parent;

	//-------------------------------------------------------------------
	public GoogleDrivePath(GoogleDriveFileSystem fs) {
		if (fs==null) throw new NullPointerException("Filesystem is null");
		this.filesystem = fs;
		
		elements = new ArrayList<>();
//		children = new HashMap<>();
		
		PathElement elem = new PathElement(Type.ROOT); 
		elem.setFileId("root");
		elements.add(elem);
//		fileId = "root";
	}

//	//-------------------------------------------------------------------
//	public GoogleDrivePath(GoogleDriveFileSystem fs, List<PathElement> elements, Map<PathElement, GoogleDrivePath> resolved) {
//		this.filesystem = fs;
//		this.elements   = elements;
//		this.children   = resolved;
//		logger.debug("  GoogleDrivePath.<FS,List,Map,Path>("+elements+","+resolved+")");
//	}

	//-------------------------------------------------------------------
	public GoogleDrivePath(GoogleDriveFileSystem fs, List<PathElement> elements) { 
//		if (elements.isEmpty())
//			throw new IllegalArgumentException("Elements may not be empty");
		this.filesystem = fs;
		this.elements   = new ArrayList<PathElement>(elements);
//		children = new HashMap<>();
//		logger.debug("  GoogleDrivePath.<FS,List,Map,Path>("+elements+","+children+")");
	}

	
//	//-------------------------------------------------------------------
//	public GoogleDrivePath(GoogleDrivePath toClone, File child) {
//		this.filesystem = toClone.filesystem;
//		this.metadata   = child;
//		this.fileId     = child.getId();
//		this.elements   = new ArrayList<>();
//		for (PathElement elem : toClone.getElements())
//			elements.add(new PathElement(elem));
//		PathElement addElem = new PathElement(child.getTitle());
//		addElem.setFileId(child.getId());
//		elements.add(addElem);
//		
//		this.children   = new HashMap<>();		
////		for (Entry<PathElement, GoogleDrivePath> res : resolved.entrySet()) {
////			resolved.put(key, value)
////		}
//		logger.debug("  GoogleDrivePath.<clone>("+toClone+")");
//	}

	//-------------------------------------------------------------------
	public boolean equals(Object o) {
		if (o instanceof GoogleDrivePath) {
			return elements.equals(  ((GoogleDrivePath)o).elements );
		}
		return false;
	}

	//-------------------------------------------------------------------
	public String getFileId() {
		if (elements.isEmpty())
			return null;
		
		return elements.get(elements.size()-1).fileId;
	}

	//-------------------------------------------------------------------
	public void setFileId(String fileId) {
		if (elements.isEmpty())
			throw new IllegalStateException("Elements may not be empty");
		
		elements.get(elements.size()-1).fileId = fileId;
//		this.fileId = fileId;
	}

	//-------------------------------------------------------------------
	public File getInternalFile() {
		if (elements.isEmpty())
			return null;
		
		return elements.get(elements.size()-1).getInternalFile();
	}

	//-------------------------------------------------------------------
	public void setInternalFile(File file) {
		if (elements.isEmpty())
			throw new IllegalStateException("Elements may not be empty");
		
		elements.get(elements.size()-1).setInternalFile(file);
	}
	
	//-------------------------------------------------------------------
	/**
	 * If neither parentKey nor parent set, path is only the filename
	 * If parent is set, is parent.toString+"/"+ filename
	 */
	@Override
	public String toString() {
		StringBuffer buf = new StringBuffer();
		
		Iterator<PathElement> it = elements.iterator();
		while (it.hasNext()) {
			PathElement elem = it.next();
			switch (elem.type) {
			case ROOT:
				buf = new StringBuffer();
				if (!it.hasNext())
					buf.append("/");
				break;
			case FILE_OR_FOLDER:
				buf.append(elem.name);
				break;
			case PARENT_REFERENCE:
				buf.append("..");
				break;
			case SELF_REFERENCE:
				buf.append(".");
				break;
			}
			if (it.hasNext())
				buf.append("/");
		}
		
//		logger.debug("toString of "+elements+" is "+buf);
		return buf.toString();
	}
	
//	//-------------------------------------------------------------------
//	private GoogleDrivePath resolve(PathElement elem) {
//		logger.debug("Resolve "+elem+" within "+fileId+"  "+elements);
//		GoogleDrivePath ret = children.get(elem);
//		if (ret!=null)
//			return ret;
//		
//		return null;
//	}

	//-------------------------------------------------------------------
	/**
	 * @see java.nio.file.Path#getFileSystem()
	 */
	@Override
	public FileSystem getFileSystem() {
		if (filesystem==null)
			logger.error("No filesystem for "+this);
		return filesystem;
	}

	//-------------------------------------------------------------------
	/**
	 * Tells whether or not this path is absolute.
	 * 
	 * An absolute path is complete in that it doesn't need to be combined with other path information in order to locate a file
	 * 
	 * @see java.nio.file.Path#isAbsolute()
	 */
	@Override
	public boolean isAbsolute() {
		for (PathElement elem : elements) {
			if (elem.type==Type.ROOT)
				return true;
		}
		return false;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.nio.file.Path#getRoot()
	 */
	@Override
	public Path getRoot() {
		logger.debug("getRoot: elements are "+elements);
		if (elements.size()==1 && elements.get(0).type==Type.ROOT)
			return this;
		if (elements.size()>1 && elements.get(0).type==Type.ROOT)
			return getParent().getRoot();
		
//		for (PathElement elem : elements) {
//			if (elem.type==Type.ROOT)
//				return new GoogleDrivePath(filesystem, elements.subList(elements.indexOf(elem), elements.indexOf(elem)));
////				return resolve(elem);
//		}
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.nio.file.Path#getFileName()
	 */
	@Override
	public Path getFileName() {
		List<PathElement> elems = new ArrayList<>();
		elems.add(getLastElement());
		return new GoogleDrivePath(filesystem, elems);
	}

//	//-------------------------------------------------------------------
//	public void setParent(GoogleDrivePath parent) {
//		this.parent = parent;
//	}

	//-------------------------------------------------------------------
	/**
	 * @see java.nio.file.Path#getParent()
	 */
	@Override
	public Path getParent() {
		if (elements.size()<1)
			return null;
		return new GoogleDrivePath(filesystem, elements.subList(0, elements.size()-1));
//		if (elements.isEmpty())
//			return null;
//		if (elements.size()<1)
//			return null;
//		
//		List<PathElement> newElements = elements.subList(0, elements.size()-1);
//		Map<PathElement, GoogleDrivePath> newResolved = new HashMap<>();
//		for (PathElement elem : newElements) {
//			logger.debug("getParent(): resolved("+resolved+").containsKey("+elem+") = "+resolved.containsKey(elem));
//			if (resolved.containsKey(elem))
//				newResolved.put(elem, resolved.get(elem));
//			else
//				logger.warn(resolved.keySet()+" does not contain "+elem);
//		}
//
//		logger.debug("Parent of "+dumpLine()+"  is  "+newElements+" // "+newResolved);
//		GoogleDrivePath ret = new GoogleDrivePath(filesystem, newElements, newResolved);
//		logger.warn("TODO: Insert real ID to "+ret);
////		// Find real file
////		filesystem.getDrive().files().list();
//		return ret;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.nio.file.Path#getNameCount()
	 */
	@Override
	public int getNameCount() {
		if (isAbsolute())
			return elements.size()-1;
		return elements.size();
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.nio.file.Path#getName(int)
	 */
	@Override
	public Path getName(int index) {
		if (elements.get(0).type==Type.ROOT)
			index++;
		List<PathElement> newElements = new ArrayList<>();
		newElements.add(elements.get(index));
		
		return new GoogleDrivePath(filesystem, newElements);
		
//		Iterator<Path> it = iterator();
//		Path last = null;
//		for (int i=0; it.hasNext() && i<=index; i++)
//			last = it.next();
//		return last;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.nio.file.Path#subpath(int, int)
	 */
	@Override
	public Path subpath(int beginIndex, int endIndex) {
		logger.warn("TODO: subpath");
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.nio.file.Path#startsWith(java.nio.file.Path)
	 */
	@Override
	public boolean startsWith(Path other) {
		// TODO Auto-generated method stub
		logger.warn("TODO. startsWith");
		return false;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.nio.file.Path#startsWith(java.lang.String)
	 */
	@Override
	public boolean startsWith(String other) {
		// TODO Auto-generated method stub
		logger.warn("TODO. startsWith");
		return false;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.nio.file.Path#endsWith(java.nio.file.Path)
	 */
	@Override
	public boolean endsWith(Path other) {
		// TODO Auto-generated method stub
		logger.warn("TODO. endsWith");
		return false;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.nio.file.Path#endsWith(java.lang.String)
	 */
	@Override
	public boolean endsWith(String other) {
		// TODO Auto-generated method stub
		logger.warn("TODO. endsWith");
		return false;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.nio.file.Path#normalize()
	 */
	@Override
	public Path normalize() {
		// TODO Auto-generated method stub
		logger.warn("TODO. normalize");
		return null;
	}

	//-------------------------------------------------------------------
	List<PathElement> getElements() {
		return elements;
	}

	//-------------------------------------------------------------------
	private PathElement getLastElement() {
		return elements.get(elements.size()-1);
	}

//	//-------------------------------------------------------------------
//	private Set<Entry<PathElement, GoogleDrivePath>> getResolved() {
//		return children.entrySet();
//	}

	//-------------------------------------------------------------------
	/**
	 * @see java.nio.file.Path#resolve(java.nio.file.Path)
	 */
	@Override
	public Path resolve(Path other) {
		logger.debug("resolve using other path: '"+other+"' in "+this);
		
		if (other.isAbsolute()) {
			if (this.getRoot()!=this)
				return this.getRoot().resolve(other);
		}
		if (other.getNameCount()==0)
			return this;
		
//		if (other.getRoot()==null) {
		
		List<PathElement> elems = new ArrayList<>(elements);
		for (Iterator<Path> it=other.iterator(); it.hasNext(); ) {
			Path elem = it.next();
			if (!elem.getFileName().toString().isEmpty())
				elems.add(new PathElement(elem.getFileName().toString()));
		}
//			List<PathElement> elems = new ArrayList<>(elements);
//			elems.addAll( ((GoogleDrivePath)other).getElements());
			GoogleDrivePath ret = new GoogleDrivePath(filesystem, elems);
			logger.debug("return "+ret.dumpLine());
			return ret;
//		}
		
	}

//	//-------------------------------------------------------------------
//	private PathElement getChildPathElement(String name) {
//		// Search for a cached entry
//		for (Entry<PathElement, GoogleDrivePath> entry : children.entrySet()) {
//			logger.debug("  compare '"+name+"' with cached '"+entry.getKey().name+"'");
//			if (entry.getKey().name!=null && entry.getKey().name.equals(name)) {
//				logger.debug("  getChildPathElement("+name+") found "+entry.getKey());
//				return entry.getKey();
//			}
//		}
//		
//		logger.debug("  getChildPathElement("+name+") found nothing");
//		return null;
//	}
//	
//	//-------------------------------------------------------------------
//	private void addChild(PathElement elem, GoogleDrivePath path) {
//		children.put(elem, path);
//	}

	//-------------------------------------------------------------------
	/**
	 * @see java.nio.file.Path#resolve(java.lang.String)
	 */
	@Override
	public Path resolve(String other) {
		logger.debug("resolve("+other+")   (this = "+dumpLine()+")");
		
		GoogleDrivePath tmp = this;
		assert filesystem!=null;
		for (String part : other.split(System.getProperty("file.separator"))) {
			PathElement elem = new PathElement(part);
			List<PathElement> newElements = new ArrayList<>(tmp.getElements());
			newElements.add(elem);
			GoogleDrivePath tmp2 = new GoogleDrivePath(filesystem, newElements);
			tmp = tmp2;
		}
		return tmp;
		
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.nio.file.Path#resolveSibling(java.nio.file.Path)
	 */
	@Override
	public Path resolveSibling(Path other) {
		logger.warn("TODO: resolveSibling");
		
		List<PathElement> newElements = new ArrayList<>(elements);
		Iterator<Path> it = other.iterator();
		while (it.hasNext()) {
			Path tmp = it.next();
			PathElement elem = new PathElement(tmp.getFileName().toString());
			newElements.add(elem);
		}
		// TODO Auto-generated method stub
		return new GoogleDrivePath(filesystem, newElements);
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.nio.file.Path#resolveSibling(java.lang.String)
	 */
	@Override
	public Path resolveSibling(String other) {
		logger.warn("TODO. resolveSibling");
		// TODO Auto-generated method stub
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.nio.file.Path#relativize(java.nio.file.Path)
	 */
	@Override
	public Path relativize(Path other) {
		logger.debug("START relativize: relativize "+other+" seen by "+this);
		int thisCount = this.getNameCount();
		int otherCount= other.getNameCount();
		logger.debug("  length this="+thisCount+"  other="+otherCount);
		
		if (this.isAbsolute() && other.isAbsolute()) {
			logger.debug("  both pathes are absolute");
			// Find identical depth
			int identicalDepth = 0;
			for (int index = 0 ; index<Math.min(thisCount, otherCount); index++) {
				Path thisPath = this.getName(index);
				Path otherPath = other.getName(index);
				logger.debug("  "+index+"  thisPath="+thisPath+"    otherPath="+otherPath);
				if (thisPath.getFileName()==null && otherPath.getFileName()==null) {
					identicalDepth++;
				} else if (thisPath.getFileName()!=null && otherPath.getFileName()!=null) {
					if (thisPath.getFileName().toString().equals(otherPath.getFileName().toString())) {
						identicalDepth++;
					} else {
						break;
					}
				} else
					break;
			}
			logger.debug("  pathes are identical until "+identicalDepth);
			
			
			List<PathElement> newElements = new ArrayList<>();
			/*
			 * Prepend .. from this path until parent is reached
			 */
			for (int i=identicalDepth; i<thisCount; i++) {
				newElements.add(new PathElement(Type.PARENT_REFERENCE));
				logger.debug("  add up ../");
			}
			// Now go down
			for (int i=identicalDepth; i<otherCount; i++) {
				Path tmp = other.getName(i);
//				ret = ret.resolve(tmp);
				logger.debug("  add down "+tmp.getFileName());
				newElements.add(new PathElement(tmp.getFileName().toString()));
			}
			GoogleDrivePath ret = new GoogleDrivePath(filesystem, newElements);
			logger.debug("STOP  relativize: Return "+ret.dumpLine());
			return ret;
		} else if (this.isAbsolute()) {
			// Only this path is absolute
		} else if (other.isAbsolute()) {
			// Only other path is absolute
		} else {
			// No path is absolute
		}
		
		logger.warn("Not implemented");
		// TODO Auto-generated method stub
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.nio.file.Path#toUri()
	 */
	@Override
	public URI toUri() {
		logger.warn("TODO: toUri");
		// TODO Auto-generated method stub
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.nio.file.Path#toAbsolutePath()
	 */
	@Override
	public Path toAbsolutePath() {
		if (isAbsolute())
			return this;
		
		logger.warn("TODO: toAbsolutePath "+this);
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.nio.file.Path#toRealPath(java.nio.file.LinkOption[])
	 */
	@Override
	public Path toRealPath(LinkOption... options) throws IOException {
		logger.warn("TODO: toRealPath");
		// TODO Auto-generated method stub
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.nio.file.Path#toFile()
	 */
	@Override
	public java.io.File toFile() {
		logger.warn("TODO: toFile");
		// TODO Auto-generated method stub
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.nio.file.Path#register(java.nio.file.WatchService, java.nio.file.WatchEvent.Kind[], java.nio.file.WatchEvent.Modifier[])
	 */
	@Override
	public WatchKey register(WatchService watcher, Kind<?>[] events, Modifier... modifiers) throws IOException {
		logger.warn("TODO: register");
		// TODO Auto-generated method stub
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.nio.file.Path#register(java.nio.file.WatchService, java.nio.file.WatchEvent.Kind[])
	 */
	@Override
	public WatchKey register(WatchService watcher, Kind<?>... events) throws IOException {
		logger.debug("TODO: register "+this+" with watch service "+watcher);
		
//		Channel channel = new Channel();
//		channel.setId(toString());
//		channel.setType("web_hook");
//		
//		try {
//		      filesystem.getDrive().files().watch(getFileId(), channel).execute();
//		    } catch (IOException e) {
//		      e.printStackTrace();
//		    }

		// TODO Auto-generated method stub
		return new GoogleDriveWatchKey(this);
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.nio.file.Path#iterator()
	 */
	@Override
	public Iterator<Path> iterator() {
		List<Path> pathList = new ArrayList<>();
		
		for (int i=0; i<elements.size(); i++) {
			GoogleDrivePath path = new GoogleDrivePath(filesystem, elements.subList(i, i+1));
			path.setFileId(elements.get(i).getFileId());
			path.setInternalFile(elements.get(i).getInternalFile());
			pathList.add(path);
		}
		
//		logger.debug("iterator returns "+pathList);
		return pathList.iterator();
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.nio.file.Path#compareTo(java.nio.file.Path)
	 */
	@Override
	public int compareTo(Path other) {
		logger.warn("TODO: compareTo");
		// TODO Auto-generated method stub
		return 0;
	}

	//-------------------------------------------------------------------
	public String dumpLine() {
		return "(elements="+elements+")";
	}

}
