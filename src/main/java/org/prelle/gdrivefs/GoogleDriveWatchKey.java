/**
 * 
 */
package org.prelle.gdrivefs;

import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.Watchable;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author prelle
 *
 */
public class GoogleDriveWatchKey implements WatchKey {
	
	private final static Logger logger = LogManager.getLogger("google.drive");

//	private GoogleDrivePath path;
	
	//-------------------------------------------------------------------
	public GoogleDriveWatchKey(GoogleDrivePath path) {
//		this.path = path;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.nio.file.WatchKey#isValid()
	 */
	@Override
	public boolean isValid() {
		// TODO Auto-generated method stub
		return false;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.nio.file.WatchKey#pollEvents()
	 */
	@Override
	public List<WatchEvent<?>> pollEvents() {
		// TODO Auto-generated method stub
		logger.warn("TODO: pollEvents");
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.nio.file.WatchKey#reset()
	 */
	@Override
	public boolean reset() {
		// TODO Auto-generated method stub
		logger.warn("TODO: reset");
		return false;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.nio.file.WatchKey#cancel()
	 */
	@Override
	public void cancel() {
		// TODO Auto-generated method stub
		logger.warn("TODO: cancel");
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.nio.file.WatchKey#watchable()
	 */
	@Override
	public Watchable watchable() {
		// TODO Auto-generated method stub
		logger.warn("TODO: watchable");
		return null;
	}

}
