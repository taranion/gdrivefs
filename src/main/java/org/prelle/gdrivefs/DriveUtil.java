/**
 * 
 */
package org.prelle.gdrivefs;

import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collections;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.api.client.http.ByteArrayContent;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.model.File;

/**
 * @author spr
 *
 */
public class DriveUtil {
	
	private final static Logger logger = LogManager.getLogger("google.drive.prov");
	
	public static void createFile(GoogleDrivePath path) throws IOException {
		Drive driveService = ((GoogleDriveFileSystem)path.getFileSystem()).getDrive();
	    GoogleDrivePath parent = (GoogleDrivePath) path.getParent();
	    String folderId = parent.getFileId();

	    /*
	     * Check if file exists
	     */
	    for (Path child : Files.newDirectoryStream(path.getParent())) {
	    	if (child.getFileName().equals(path.getFileName()))
	    		throw new FileAlreadyExistsException(path.toString());
	    }
	    
		// File's metadata.
	    File fileMetadata = new File();
	    fileMetadata.setName(path.getFileName().toString());
	    fileMetadata.setMimeType("application/octet-stream");
	    
	    // Set the parent folder.
	    if (folderId!= null && folderId.length() > 0) {
	    	fileMetadata.setParents(Collections.singletonList(folderId));
	    }

	    // File's content.
	    ByteArrayContent mediaContent = new ByteArrayContent("application/octet-stream", new byte[0]);
//	    FileContent mediaContent = new FileContent("application/octet-stream", tmp.toFile());
	    logger.debug("Call Files.create in folder "+folderId);
	    File file = driveService.files().create(fileMetadata, mediaContent)
	    		.setFields("id, parents, createdTime, modifiedTime")
	    		.execute();
	    path.setInternalFile(file);
	    path.setFileId(file.getId());
	    logger.debug("Created "+file.getId());

	}

}
