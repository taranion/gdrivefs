/**
 * @author prelle
 *
 */
module gdrive.fs {
	exports org.prelle.gdrivefs;

	requires transitive google.api.client;
	requires google.api.services.drive.v3.rev101;
	requires google.http.client;
	requires google.http.client.jackson2;
	requires google.oauth.client;
	requires google.oauth.client.java6;
	requires google.oauth.client.jetty;
	requires java.prefs;
	requires junit;
	requires org.apache.logging.log4j;
}